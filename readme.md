# LeadDesk REST API PHP Examples

This project contains PHP 7.x source code examples for using the LeadDesk REST API. All the examples should be runnable from the command line if you first edit your own API and user credentials into them. See each source code file for more detailed instructions on how to configure them properly.

## Initial configuration

First you need to download some dependencies. If you don't have composer installed, then please follow instructions from here: https://getcomposer.org/download/.

After you have composer installed then go ahead and install all the dependencies by running this from the project root folder:
```
composer install
```

# Examples

All the code samples require you to add your own API credentials and other configuration. So please don't try to run them before you have modified them accordingly.

## Authentication

![Authorization Flow](doc/rest-api-oauth-flow.png)

*Password Grant*

```
php src/password-grant.php
```

*LeadDesk Client Id Grant*

```
php src/leaddesk-client-id-grant.php
```

*LeadDesk Client User Grant*

```
php src/client-user-grant.php
```

*Refresh Grant*

```
php src/refresh-grant.php
```

## Using Resources After Authentication

*Create, Modify and Delete an Activity*

```
php src/create-modify-delete-activity.php
```

# Embed Examples from BitBucket

How to embed code snippets from BitBucket into other web pages.

```
<!doctype html>
<html>
<head>
    <style>
        .bb-source-embed span {
            float: none !important;
        }
    </style>
</head>
<body>
    <script src="https://bitbucket.org/leaddesk/rest-api-examples/src/master/src/client-user-grant.php?embed=t"></script>
</body>
</html>
```
