<?php
/**
 * This example source code will demonstrate the usage of resources after authentication.
 *
 * You need:
 * - valid access token
 *
 * We will be skipping the authentication part and expect to have a functional access token
 * already. If you want to run the code then replace the $accessToken variable with a real
 * access token.
 *
 * Steps:
 *  1. We will create a new activity named 'example activity'
 *  2. We will change the name of that activity to 'modified activity name'
 *  3. Finally we will delete that activity
 *
 * @see https://bitbucket.org/leaddesk/rest-api-examples for information on dependencies etc.
 */
require(__DIR__ . '/../vendor/autoload.php');

use GuzzleHttp\Client;
use Symfony\Component\Console\Output\ConsoleOutput;

$console = new ConsoleOutput;

try {
    // We skip the authentication part and expect to have a valid access token already
    $accessToken = 'icUL58r9fn0y4zWEMlZWNvaRmNt4Kx4Ixms3EOcr';
    $apiClient = new Client([
        // Rest API base URL
        'base_uri' => 'https://api.cloud.leaddesk.com/stable/',
        'headers' => [
            'Authorization' => "Bearer {$accessToken}",
        ],
    ]);

    // Create a new activity with a POST request
    // POST /activities
    $createActivityResponse = $apiClient->request('POST', 'activities', [
        'json' => [
            'name' => 'example activity',
            'billable' => true,
            'payable' => true,
        ],
    ]);
    $activity = json_decode($createActivityResponse->getBody());
    $console->writeln("Newly created activity id: {$activity->id}");

    // Modify the activity's name we just created with a PATCH request
    // PATCH /activities/{id}
    $apiClient->request('PATCH', "activities/{$activity->id}", [
        'json' => [
            'name' => 'modified activity name',
        ],
    ]);
    $console->writeln("Activity name changed: OK");

    // Finally delete the activity we have just created with a DELETE request
    // DELETE /activities/{id}
    $apiClient->request('DELETE', "activities/{$activity->id}");
    $console->writeln("Activity deleted: OK");
} catch (Exception $e) {
    $console->writeln("<error>{$e->GetMessage()}</error>");
}
