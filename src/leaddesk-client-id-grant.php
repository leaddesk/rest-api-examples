<?php
/**
 * This Example source code will demonstrate how to authenticate with the "leaddesk_client_id" grant.
 *
 * You need:
 * - API credentials: client_id + client_secret
 * - Existing LeadDesk account id: leaddesk_client_id
 *
 * Just replace the example's credentials below with your own and you can run this code. This
 * code will just read and print out the users from the authenticated LeadDesk account.
 *
 * The LeadDesk Client Id grant gives you an access token that allows you to do actions with the REST API
 * that have been configured to your API client account. You might have full access to the client or
 * just limited access (e.g. read-only access).
 *
 * This authorization method is NOT enabled for all LeadDesk REST API users. Using this authentication
 * method requires that your API account is given permission to use it for your LeadDesk account.
 *
 * This authentication method is most suited for automated tasks that need to be done without
 * knowing any user's username and password. It can also be used to minimize the access rights within
 * the API for the API client. For example, if the automated task only needs to manage activities
 * then it can be configured to not have permission to access any other resources.
 *
 * @see https://bitbucket.org/leaddesk/rest-api-examples for information on dependencies etc.
 */
require(__DIR__ . '/../vendor/autoload.php');

use GuzzleHttp\Client;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Helper\Table;

$console = new ConsoleOutput;

try {
    $apiClient = new Client([
        // Rest API base URL
        'base_uri' => 'https://api.cloud.leaddesk.com/stable/',
    ]);

    // Authenticate
    $tokensResponse = $apiClient->request('POST', 'oauth/access-token', [
        'json' => [
            'grant_type' => 'leaddesk_client_id',
            // Rest API credentials
            'client_id' => 'leaddesk.examples',
            'client_secret' => '69RqIxU1iUa_CLIENT_SECRET_jqfCqQL2dSD9ZW',
            // LeadDesk client selection
            'leaddesk_client_id' => 134,
        ]
    ]);
    $tokens = json_decode($tokensResponse->getBody());

    // GET /users
    $usersResponse = $apiClient->request('GET', 'users', [
        'headers' => [
            'Authorization' => "Bearer {$tokens->access_token}",
        ],
    ]);
    $users = json_decode($usersResponse->getBody());

    // Print out the list of users in a table format
    $table = new Table($console);
    $table->setHeaders(['id', 'username', 'role', 'full name']);
    foreach ($users->collection as $user) {
        $table->addRow([$user->id, $user->username, $user->role, $user->name]);
    }
    $table->render();
} catch (Exception $e) {
    $console->writeln("<error>{$e->GetMessage()}</error>");
}
