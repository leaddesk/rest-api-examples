<?php
/**
 * This Example source code will demonstrate how to authenticate with the "password" grant.
 *
 * You need:
 * - API credentials: client_id + client_secret
 * - Existing LeadDesk user: username + password
 *
 * Just replace the example's credentials below with your own and you can run this code. This
 * code will just read and print out the users from the authenticated LeadDesk account.
 *
 * The Password grant gives you an access token that allows you to do actions with the REST API
 * as the user you used when authenticating. User role restrictions will apply. For example,
 * a team-leader user is not allowed to do or see as much as an admin user is allowed to.
 *
 * This is the most basic authentication method and can be used to easily automate tasks
 * for your own LeadDesk client. This authorization method is enabled for all LeadDesk REST API users.
 *
 * It's not recommended to use this for 3rd party LeadApps because users would need to give out their
 * personal credentials to outsiders.
 *
 * @see https://bitbucket.org/leaddesk/rest-api-examples for information on dependencies etc.
 */
require(__DIR__ . '/../vendor/autoload.php');

use GuzzleHttp\Client;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Helper\Table;

$console = new ConsoleOutput;

try {
    $apiClient = new Client([
        // Rest API base URL
        'base_uri' => 'https://api.cloud.leaddesk.com/stable/',
    ]);

    // Authenticate
    $tokensResponse = $apiClient->request('POST', 'oauth/access-token', [
        'json' => [
            'grant_type' => 'password',
            // Rest API credentials
            'client_id' => 'leaddesk.examples',
            'client_secret' => '69RqIxU1iUa_CLIENT_SECRET_jqfCqQL2dSD9ZW',
            // LeadDesk user credentials
            'username' => 'john.doe.admin',
            'password' => 'MMtG39jk0a8f',
        ]
    ]);
    $tokens = json_decode($tokensResponse->getBody());

    // GET /users
    $usersResponse = $apiClient->request('GET', 'users', [
        'headers' => [
            'Authorization' => "Bearer {$tokens->access_token}",
        ],
    ]);
    $users = json_decode($usersResponse->getBody());

    // Print out the list of users in a table format
    $table = new Table($console);
    $table->setHeaders(['id', 'username', 'role', 'full name']);
    foreach ($users->collection as $user) {
        $table->addRow([$user->id, $user->username, $user->role, $user->name]);
    }
    $table->render();
} catch (Exception $e) {
    $console->writeln("<error>{$e->GetMessage()}</error>");
}
