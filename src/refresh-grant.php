<?php
/**
 * This example source code will demonstrate how to refresh an expired access token.
 *
 * You need:
 * - API credentials: client_id + client_secret
 * - Existing LeadDesk user: username + password
 *
 * Just replace the example's credentials below with your own and you can run this code. This
 * code will first authenticate with the password grant and then a new access token is requested by using
 * the refresh token.
 *
 * The Refresh Token grant allows you to get a new access token with all the same permissions as the original
 * access token. It can be used to get a new access token when the previous one is not valid anymore.
 * Access tokens normally expire after an hour. Refresh tokens are valid for a much longer time.
 * The Refresh Token grant can also be used to invalidate an existing access token and get a new one instead.
 * The refresh token will become invalid once used.
 *
 * This grant method is enabled for all LeadDesk REST API users.
 *
 * @see https://bitbucket.org/leaddesk/rest-api-examples for information on dependencies etc.
 */
require(__DIR__ . '/../vendor/autoload.php');

use GuzzleHttp\Client;
use Symfony\Component\Console\Output\ConsoleOutput;

$console = new ConsoleOutput;

try {
    $apiClient = new Client([
        // Rest API base URL
        'base_uri' => 'https://api.cloud.leaddesk.com/stable/',
    ]);

    // Authenticate first with the password grant (see the password grant example)
    $tokensResponse = $apiClient->request('POST', 'oauth/access-token', [
        'json' => [
            'grant_type' => 'password',
            // Rest API credentials
            'client_id' => 'leaddesk.examples',
            'client_secret' => '69RqIxU1iUa_CLIENT_SECRET_jqfCqQL2dSD9ZW',
            // LeadDesk user credentials
            'username' => 'john.doe.admin',
            'password' => 'MMtG39jk0a8f',
        ]
    ]);
    $tokens = json_decode($tokensResponse->getBody());
    $console->writeln("access_token[1]: {$tokens->access_token}");

    //
    // ... do some very long operation after which the access token has expired ...
    // ... or maybe we just want to invalidate the previous access token ...
    //

    // Use the refresh grant to get a new access token
    $refreshResponse = $apiClient->request('POST', 'oauth/access-token', [
        'json' => [
            'grant_type' => 'refresh_token',
            // Rest API credentials
            'client_id' => 'leaddesk.examples',
            'client_secret' => '69RqIxU1iUa_CLIENT_SECRET_jqfCqQL2dSD9ZW',
            // Refresh token from the previous authentication request
            'refresh_token' => $tokens->refresh_token,
        ]
    ]);
    $newTokens = json_decode($refreshResponse->getBody());
    $console->writeln("access_token[2]: {$newTokens->access_token}");

    // Continue API usage normally with the new access token
    $usersResponse = $apiClient->request('GET', 'users', [
        'headers' => [
            'Authorization' => "Bearer {$newTokens->access_token}",
        ],
    ]);

    // ...
} catch (Exception $e) {
    $console->writeln("<error>{$e->GetMessage()}</error>");
}
